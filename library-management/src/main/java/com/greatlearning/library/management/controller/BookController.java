package com.greatlearning.library.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.greatlearning.library.management.entity.Book;
import com.greatlearning.library.management.service.BookService;

@Controller
@RequestMapping("/books")
public class BookController {
	@Autowired
	private BookService bookService;

	@RequestMapping("/list")
	public String listBooks(Model model) {
		model.addAttribute("books", bookService.findAll());
		return "book-list";
	}
	@RequestMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {
		model.addAttribute("mode", "Add");
		model.addAttribute("book", new Book());
		return "book-form";
	}
	@RequestMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("bookId") int id, Model model) {
		model.addAttribute("mode", "Update");
		model.addAttribute("book", bookService.findById(id));
		return "book-form";
	}
	@PostMapping("/save")
	public String saveBook(@RequestParam("id") int id, 	
			@RequestParam("title") String title, 
			@RequestParam("author") String author, 
			@RequestParam("category") String category) {
		Book book = null;
		if (id == 0) {
			book = new Book(title, author, category);
		} else {
			book = bookService.findById(id);
			book.setTitle(title);
			book.setAuthor(author);
			book.setCategory(category);
		}
		bookService.save(book);
		return "redirect:list";
	}
	@RequestMapping("/delete")
	public String delete(@RequestParam("bookId") int id) {
		bookService.deleteById(id);
		return "redirect:list";
	}
	@RequestMapping("/search")
	public String search(@RequestParam("title") String title, 	@RequestParam("author") String author, Model model) {
		model.addAttribute("books", bookService.searchBy(title, author));
		return "book-list";
	}
}
