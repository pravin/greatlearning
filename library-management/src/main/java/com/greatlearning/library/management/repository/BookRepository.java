package com.greatlearning.library.management.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.greatlearning.library.management.entity.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {
	public List<Book> findByTitleContainsAndAuthorContainsAllIgnoreCase(String title,String author);
}
