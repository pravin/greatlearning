package com.greatlearning.algorithms.main;

import java.util.Arrays;
import java.util.Scanner;

import com.greatlearning.algorithms.util.ArrayUtility;

public class DriverClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] array = new int[n];
		for (int i = 0; i < n; i++) {
			array[i] = sc.nextInt();
		}
		System.out.println("original array :"+Arrays.toString(array));
		ArrayUtility.sort(array);
		System.out.println("sorted array :"+Arrays.toString(array));
		ArrayUtility.rotateLeft(array, array.length / 2);
		System.out.println("rotated array :"+Arrays.toString(array));
		for (int i = 0; i < 5; i++) {
			int key = sc.nextInt();
			int keyIndex = ArrayUtility.searchInRotatedArray(array, 0, array.length-1, key);
			System.out.println(keyIndex == -1?"key "+key+" not found in array":
						"key "+ key + " found at index "+keyIndex);
		}
		sc.close();
	}
}
