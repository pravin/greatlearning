package com.greatlearning.algorithms.util;

public class ArrayUtility {
	public static void sort(int[] array) {
		mergeSort(array, 0, array.length-1);
	}
	public static void mergeSort(int[] array, int low, int high) {
//		Arrays.sort(array, low, high+1);
		// TODO write the mergeSort algorithm implementation
		if (low < high) {
			int mid = (low + high) / 2;
			mergeSort(array, low, mid);
			mergeSort(array, mid+1, high);
			
			merge(array, low, mid, high);
		}
		
	}
/*	
	private class MyRotatedArray {
		private int[] array;
		private int rotation;
		private boolean left;
		public int getValue(int index) {
			return array[(index + rotation)%array.length]; 
		}
	}
	MyRotatedArray myArray = new MyRotatedArray(array, 4);
	myArray.getValue(i);
*/
	private static void merge(int[] array, int low, int mid, int high) {

		int n1 = mid - low + 1;
		int n2 = high - mid;
		System.out.println("n1:"+n1+" n2:"+n2+" low:"+low+" mid:"+mid+" high:"+high);
		int[] leftArray = new int[n1];
		int[] rightArray = new int[n2];
		for (int i = 0; i < n1; i++) {
			leftArray[i] = array[low+i];
		}
		for (int i = 0; i < n2; i++) {
			rightArray[i] = array[mid+1+i];
		}
/*
	244 353 435 34 234 44	
		int[] leftArray = Arrays.copyOfRange(array, low, low+n1); 
		int[] rightArray = Arrays.copyOfRange(array, mid+1, mid+1+n2);
		System.out.println("left array:"+Arrays.toString(leftArray));
		System.out.println("right array:"+Arrays.toString(rightArray));
*/
		int i = 0, j = 0;
		int k = low;
		while (i < n1 && j < n2) {
			if (leftArray[i] <= rightArray[j]) {
				array[k++] = leftArray[i++];
			}
			else {
				array[k++] = rightArray[j++];
			}
		}
		while (i < n1) {
			array[k++] = leftArray[i++];
		}
		while (j < n2) {
			array[k++] = rightArray[j++];
		}
	}
	public static void rotateLeft(int[] array, int numberOfTimes) {
		for (int i = 0; i < numberOfTimes; i++) {
			rotateLeftOnce(array);
		}
	}
	public static void rotateLeftOnce(int[] array) {
		// { 2, 54, 56,89, 100}
		// after rotation left 1 time
		// {54, 56, 89, 100, 2}
		int value = array[0];
		for (int i = 0; i < array.length - 1; i++) {
			array[i] = array[i+1];
		}
		array[array.length-1] = value;
	}
	public static int binarySearch(int[] array, int low, int high, int key) {
		if (low > high) {	// not found, so returning -1
			return -1;
		}
		int mid = (low + high) / 2;
		if (array[mid] == key) {
			return mid;
		}
		return array[mid] > key ? binarySearch(array, low, mid-1, key): // first half
				binarySearch(array, mid+1, high, key);	// second half
	}
	public static int searchInRotatedArray(int[] array, int low, int high, int key) {
		int pivot = findPivot(array, 0, array.length-1);
		if (pivot == -1) {
			return -1;
		}
		if (array[pivot] == key) {
			return pivot;
		}
		// [244, 353, 435, 543, 643, 34, 37, 42, 54, 75, 234]
		return array[low] < key ? binarySearch(array, low, pivot-1, key) :
				binarySearch(array, pivot+1, high, key);
	}
	// [244, 353, 435, 34, 234]
	// find pivot in a rotated array
	public static int findPivot(int[] array, int low, int high) {
		if (low > high) {
			return -1;
		}
		int mid = (low + high) / 2;  
		if (low < mid && array[mid] > array[mid+1]) {
			return mid;
		}
		if (low < mid && array[mid-1] > array[mid]) {
			return mid-1;
		}
		// [244, 353, 435, 543, 643, 34, 234]
		return array[low] < array[mid] ? findPivot(array, mid+1, high) :
					findPivot(array, low, mid-1);
	}
}
