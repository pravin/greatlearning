package com.greatlearning.iitr.dsa.main;

import java.util.Scanner;

import com.greatlearning.iitr.dsa.model.StackUsingTwoQueues;

public class DriverClass {

    public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
//		StackUsingTwoQueues<Integer> stackUsingQueue = new StackUsingTwoQueues<>();
		StackUsingTwoQueues<String> stackUsingQueue = new StackUsingTwoQueues<>();
		System.out.println("Stack Using Two Queues \n");
		char ch;
		do {
			System.out.println("Stack Operations");
			System.out.println("1. push");
			System.out.println("2. pop");
			System.out.println("3. check empty");
			System.out.println("4. size");
			int choice = scanner.nextInt();
			switch (choice) {
			case 1:
				System.out.println("Enter integer element to push");
//				stackUsingQueue.push(scanner.nextInt());
				stackUsingQueue.push(scanner.next());
				break;
			case 2:
				try {
				} catch (Exception e) {
					System.out.println("Error : " + e.getMessage());
				}
				break;
			case 3:
				System.out.println("Empty status = " + stackUsingQueue.isEmpty());
				break;
			case 4:
				System.out.println("Size = " + stackUsingQueue.getSize());
				break;
			default:
				System.out.println("Enter a valid option \n ");
				break;
			}
			
			stackUsingQueue.display();
			System.out.println("Do you want to continue (Type y or n) \n");
			ch = scanner.next().charAt(0);
		} while (ch == 'Y' || ch == 'y');
		scanner.close();
    }
}
