package com.greatlearning.iitr.dsa.model;

import java.util.LinkedList;
import java.util.Queue;

public class StackUsingTwoQueues<T> {
	private Queue<T>[] queue = new Queue[] {
		new LinkedList<>(),
		new LinkedList<>()
	};
	private int currentIndex = 0;

/*	Initializer block, called whenever any creates a new object of this class
	{
		queue[0] = new LinkedList<>();
		queue[1] = new LinkedList<>();
	}
	public StackUsingTwoQueues() {
		queue = new Queue<T>[2];
		queue[0] = new LinkedList<>();
		queue[1] = new LinkedList<>();
	}
*/
	public int getSize() {
		return queue[currentIndex].size();
	}
	public void push(T value) {
		int nextIndex = (currentIndex+1)%2;
		queue[nextIndex].add(value);
//		int size = queue[currentIndex].size();
//		for (int i = 0; i < size; i++) {
		T element = null;
		while ((element = queue[currentIndex].poll())!=null) {
			queue[nextIndex].add(element);
		}
		currentIndex = nextIndex;
	}
	public T pop() {
		return queue[currentIndex].remove();
	}
	public T peek() {
		return queue[currentIndex].peek();
	}
	public boolean isEmpty() {
		return queue[currentIndex].isEmpty();
	}
	public void display() {
		System.out.println(queue[currentIndex]);
	}
}
