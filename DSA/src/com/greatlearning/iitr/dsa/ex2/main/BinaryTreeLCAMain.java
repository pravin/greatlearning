package com.greatlearning.iitr.dsa.ex2.main;

import java.util.Scanner;

import com.greatlearning.iitr.dsa.ex2.model.BinaryTree;
import com.greatlearning.iitr.dsa.ex2.model.BinaryTree.Node;

public class BinaryTreeLCAMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	BinaryTree tree = new BinaryTree();
        tree.root(new Node(10));
        tree.root().left(new Node(20)).right(new Node(30));
//        tree.root().right(new Node(30));
        tree.root().left().left(new Node(40)).right(new Node(50));
//        tree.root().left().right(new Node(50));
        tree.root().right().left(new Node(60)).right(new Node(70));
//        tree.root().right().right(new Node(70));
 
        System.out.println("Least Common Ancestor(20, 30): " + tree.findLCA(20,30));
        System.out.println("Least Common Ancestor(40, 30): " + tree.findLCA(40,30));
        System.out.println("Least Common Ancestor(60, 70): " + tree.findLCA(60,70));
        System.out.println("Least Common Ancestor(20, 40): " + tree.findLCA(20,40));
        Scanner scanner = new Scanner(System.in);
        System.out.println("do you wnat to continue (y/n):");
        String choice = scanner.next();
        while (choice.equalsIgnoreCase("y")) {
            System.out.println("Enter value for node 1");
            int node1Value = scanner.nextInt();
            System.out.println("Enter value for node 2");
            int node2Value = scanner.nextInt();
            System.out.println("Least Common Ancestor("+node1Value+", "+node2Value+"): " + tree.findLCA(node1Value,node2Value));
            System.out.println("do you wnat to continue (y/n):");
            choice = scanner.next();
        }
        scanner.close();
	}

}
