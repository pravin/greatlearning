package com.greatlearning.iitr.dsa.ex2.model;

import java.util.ArrayList;
import java.util.List;

// A Binary Tree node
/*
    Node n = new Node(54);
    Node n1 = n.left(new Node(34)).right(new Node(75));
    if (n == n1)
*/
public class BinaryTree {
    public static class Node {
        private int data;
	    private Node left, right;
	 
	    public Node(int value) {
	        data = value;
	        left = right = null;
	    }
	    public Node left() {
	        return this.left;
	    }
	    public Node right() {
	        return this.right;
	    }
	    public Node left(Node newNode) {
	        this.left = newNode;
	        return this;
	    }
	    public Node right(Node newNode) {
	        this.right = newNode;
	        return this;
	    }
	}

	private Node root;

    public Node root() {
        return this.root;
    }
    public Node root(Node root) {
        this.root = root;
        return this.root;
    }
    // Finds the path from root node to given root of the tree.
    public int findLCA(int node1, int node2) {
        List<Integer> path1 = new ArrayList<>();
        List<Integer> path2 = new ArrayList<>();
        return findLCAInternal(root, node1, node2, path1, path2);
    }

    private int findLCAInternal(Node root, int node1, int node2, List<Integer> path1, List<Integer> path2) {

        if (!findPath(root, node1, path1) || !findPath(root, node2, path2)) {
            System.out.println((path1.size() > 0) ? "node1 is present" : "node1 is missing");
            System.out.println((path2.size() > 0) ? "node2 is present" : "node2 is missing");
            return -1;
        }
        int i; // not declared in for, but prior to for loop
        	   // since it is used in the return statement outside for loop
        for (i = 0; i < path1.size() && i < path2.size(); i++) {

        // System.out.println(path1.get(i) + " " + path2.get(i));
            if (!path1.get(i).equals(path2.get(i)))
                break;
        }

        return path1.get(i-1);
    }

    // Finds the path from root node to given root of the tree, Stores the
    // path in a vector path[], returns true if path exists otherwise false
    private boolean findPath(Node root, int n, List<Integer> path) {
        // base case
        if (root == null) {
            return false;
        }

        // Store this node . The node will be removed if
        // not in path from root to n.
        path.add(root.data);

        if (root.data == n) {
            return true;
        }

        if (root.left != null && findPath(root.left, n, path)) {
            return true;
        }

        if (root.right != null && findPath(root.right, n, path)) {
            return true;
        }

        // If not present in subtree rooted with root, remove root from
        // path[] and return false
        path.remove(path.size()-1);

        return false;
    }
}
