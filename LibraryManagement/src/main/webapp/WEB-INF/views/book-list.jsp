<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Library Management System - books</title>
</head>
<body>
<h3>Search book</h3>
<form action="search" method="post">
	<input type="search" name="title" placeholder="Title">
	<input type="search" name="author" placeholder="Author">
	<input type="submit" value="Search Book">
</form>
<h3>Add new book</h3>
<a href="addNewBook">New Book</a>
<%--
<form action="addNewBook" method="post">
	<input type="text" name="title" placeholder="Title">
	<input type="text" name="author" placeholder="Author">
	<input type="text" name="category" placeholder="Category">
	<input type="submit" value="Add Book">
</form>
 --%>
<h1>List of books</h1>
<table>
<thead>
	<tr>
		<th>Title</th>
		<th>Author</th>
		<th>Category</th>
		<th>Action</th>
	</tr>
</thead>
<c:forEach items="${books}" var="bookObj">
	<tr>
		<td>${bookObj.title}</td>
		<td>${bookObj.author}</td>
		<td>${bookObj.category}</td>
		<td><a href="updateBook?id=${bookObj.id}">Update</a>
			<a href="deleteBook?id=${bookObj.id}">Delete</a></td>
	</tr>
</c:forEach>
</table>
</body>
</html>