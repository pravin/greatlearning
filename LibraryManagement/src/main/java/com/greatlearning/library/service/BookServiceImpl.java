package com.greatlearning.library.service;

import java.util.List;
import java.util.StringJoiner;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.greatlearning.library.model.Book;

@Repository // equivalent of @Component
public class BookServiceImpl implements BookService {

	private SessionFactory factory;

	private Session session;

	@Autowired
	public BookServiceImpl(SessionFactory factory) {
		this.factory = factory;
		try {
			this.session = factory.getCurrentSession();
		} catch (HibernateException e) {
			this.session = factory.openSession();
		}
	}
	@Override
	public List<Book> getAllBooks() {
		// TODO Auto-generated method stub
		List<Book> books = session.createQuery("from Book").list();
		
		return books;
	}

	@Override
	public Book getBook(int bookId) {
		// TODO Auto-generated method stub
		Book book = session.get(Book.class, bookId);
		return book;
	}

	@Override
	@Transactional
	public void save(Book book) {
		// TODO Auto-generated method stub
//		Book book = new Book("Java Programming", "Author", "Programming");
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(book);
		tx.commit();
	}

	@Transactional
	@Override
	public void deleteBook(int bookId) {
		// TODO Auto-generated method stub
		Transaction tx = session.beginTransaction();
		Book book = session.get(Book.class, bookId);
		session.delete(book);
		tx.commit();
	}

	@Override
	public List<Book> searchBook(String title, String author) {
		// TODO Auto-generated method stub
		String queryFirstPart = "from Book where ";
		String searchTitle = "title like '%"+title+"%'";
		String searchAuthor = "author like '%"+author+"%'";
		StringJoiner stringJoiner = new StringJoiner(" or ",queryFirstPart,"");
		stringJoiner.setEmptyValue("from Book");
		if (!title.trim().isEmpty()) {
			stringJoiner.add(searchTitle);
		}
		if (!author.trim().isEmpty()) {
			stringJoiner.add(searchAuthor);
		}
		String queryString = stringJoiner.toString();
		List<Book> books = session.createQuery(queryString).list();
		return books;
	}

}
