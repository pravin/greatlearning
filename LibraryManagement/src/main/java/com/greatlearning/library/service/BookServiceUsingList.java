package com.greatlearning.library.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.greatlearning.library.model.Book;

public class BookServiceUsingList implements BookService {

	private int lastBookId;
	private List<Book> books = new ArrayList<>();

	@Override
	public List<Book> getAllBooks() {
		// TODO Auto-generated method stub
		return Collections.unmodifiableList(books);
	}

	@Override
	public Book getBook(int bookId) {
		// TODO Auto-generated method stub
		return books.get(bookId-1);
	}

	@Override
	public void save(Book book) {
		// TODO Auto-generated method stub
		if (book.getId() == 0) {
			book.setId(++lastBookId);
			books.add(book);
			return;
		}
		books.set(book.getId()-1, book);
	}

	@Override
	public void deleteBook(int bookId) {
		// TODO Auto-generated method stub
		books.set(bookId, null);
	}

	@Override
	public List<Book> searchBook(String title, String author) {
		// TODO Auto-generated method stub
		Set<Book> searchBooks = new HashSet<>();
		if (title.trim().isEmpty() && author.trim().isEmpty()) {
			return new ArrayList<>(searchBooks);
		}
		if (!title.trim().isEmpty()) {
			for (Book book : books) {
				if (book == null) {
					continue;
				}
				if (book.getTitle().contains(title)) {
					searchBooks.add(book);
				}
			}
		}
		if (!author.trim().isEmpty()) {
			for (Book book : books) {
				if (book == null) {
					continue;
				}
				if (book.getAuthor().contains(author)) {
					searchBooks.add(book);
				}
			}
		}
		return new ArrayList<>(searchBooks);
	}

}
