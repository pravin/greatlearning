package com.greatlearning.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.greatlearning.library.model.Book;
import com.greatlearning.library.service.BookService;

@Controller
@RequestMapping("/book")	// http(s)://<host>/<projectname>/book
public class BookController {
	@Autowired
	private BookService bookService;

	@RequestMapping("/list")	// http(s)://<host>/<projectname>/book/list
	public String showBooks(Model model) {
		List<Book> books = bookService.getAllBooks();
		model.addAttribute("books", books);
		return "book-list";	//	/WEB-INF/views/book-list.jsp
	}
	@RequestMapping("/addNewBook")
	public String addNewBook(Model model) {
		Book book = new Book();
		model.addAttribute("bookObj", book);
		return "book-form";
	}
	@RequestMapping("/updateBook")	// /LibraryManagement/book/updateBook?id=2
	public String updateBook(@RequestParam(name = "id") int bookdId, Model model) {
		Book book = bookService.getBook(bookdId);
		model.addAttribute("bookObj", book);
		return "book-form";
	}
	@PostMapping("/save")
	public String saveBook(@RequestParam(name = "id") int bookId, 
			@RequestParam(name = "title") String title,
			@RequestParam(name = "author") String author,
			@RequestParam(name = "category") String category
			) {
		Book book = null;
		if (bookId == 0) {
			book = new Book(title, author, category);
		} else {
			book = bookService.getBook(bookId);
			book.setTitle(title);
			book.setAuthor(author);
			book.setCategory(category);
		}
		bookService.save(book);
		return "redirect:list";
	}
	@RequestMapping("/deleteBook")
	public String deleteBook(@RequestParam(name="id")int bookId) {
		bookService.deleteBook(bookId);
		return "redirect:list";	// LibraryManagement/book/list
	}
	@PostMapping("/search")	// /book/search title and author
	public String searchBook(@RequestParam(name = "title") String title,
			@RequestParam(name = "author") String author,
			Model model
			) {
		List<Book> books = bookService.searchBook(title, author);
		model.addAttribute("books", books);
		return "book-list";
	}
}
