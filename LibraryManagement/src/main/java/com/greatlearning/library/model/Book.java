package com.greatlearning.library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Books")
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "title", length = 150)
	private String title = "";
	// if use title.length(), if title is null, it will be an error
	// if title is "" it won't give error, but returns 0
	// isEmpty()	title.isEmpty(), title.trim() 
	@Column(name = "author", length = 100)
	private String author = "";
	@Column(name = "category")
	private String category = "";
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book(String title, String author, String category) {
		super();
		this.title = title;
		this.author = author;
		this.category = category;
	}
// id is a property
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
// property title
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
// property author if we have an object called book,
//  ${book.author}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", author=" + author + ", category=" + category + "]";
	}
	
}
