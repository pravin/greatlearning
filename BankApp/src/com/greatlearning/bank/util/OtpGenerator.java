package com.greatlearning.bank.util;

import java.util.Random;

public class OtpGenerator {
	public static String generateOtp() {
		int number = new Random().nextInt(10_000);
		return String.format("%4d", number);
	}
}
