package com.greatlearning.bank.model;

public class BankAccount {
	private String accountNumber;
	private String password;
	private double balance;
	
	public BankAccount(String acno, String passwd, double openBalance) {
		this.accountNumber = acno;
		this.password = passwd;
		this.balance = openBalance;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public double getBalance() {
		return balance;
	}
	public void deposit(double amount) {
		this.balance += amount;
	}
	public boolean withdraw(double amount) {
		if (amount > this.balance) {
			return false;
		}
		this.balance -= amount;
		return true;
	}
}
