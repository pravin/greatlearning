package com.greatlearning.bank.service;

import com.greatlearning.bank.model.BankAccount;

public class BankingService {
	private BankAccount[] bankAccounts = new BankAccount[2];
	{
		bankAccounts[0] = new BankAccount("23423441","pass1", 1_00_000);
		bankAccounts[1] = new BankAccount("22345461","pass2", 1_00_000);
	}
	private BankAccount getAccount(String acno) {
		switch (acno) {
		case "23423441":
			return bankAccounts[0];
		case "22345461":
			return bankAccounts[1];
		default:
			return null;
		}
	}
	public boolean authenticate(String acno, String passwd) {
		BankAccount account = getAccount(acno);
		if (account == null) {
			return false;
		}
		if (account.getPassword().equals(passwd)) {
			return true;
		}
		return false;
	}
	public void deposit(String acno, double amount) {
		BankAccount account = getAccount(acno);
		if (account == null) {
			System.out.println("invalid account number"+acno);
		}
		account.deposit(amount);
		
	}
	public boolean withdraw(String acno, double amount) {
		BankAccount account = getAccount(acno);
		if (account == null) {
			System.out.println("invalid account number"+acno);
		}
		return account.withdraw(amount);
	}
	public boolean transfer(String acno, String destAcno, double amount) {
		BankAccount account = getAccount(acno);
		if (account == null) {
			System.out.println("invalid account number"+acno);
		}
		BankAccount destAccount = getAccount(destAcno);
		if (destAccount == null) {
			System.out.println("invalid account number"+destAcno);
		}
		boolean status = withdraw(acno, amount);
		if (!status) {
			return false;
		}
		deposit(destAcno, amount);
		return true;
	}
	public double getBalance(String acno) {
		BankAccount account = getAccount(acno);
		if (account == null) {
			System.out.println("invalid account number"+acno);
		}
		return account.getBalance();
	}
}
