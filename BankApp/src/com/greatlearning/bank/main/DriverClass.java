package com.greatlearning.bank.main;

import java.util.Scanner;

import com.greatlearning.bank.model.BankAccount;
import com.greatlearning.bank.service.BankingService;
import com.greatlearning.bank.util.OtpGenerator;

public class DriverClass {
/*
 * 		bankAccounts[0] = new BankAccount("23423441","pass1", 1_00_000);
 *		bankAccounts[1] = new BankAccount("22345461","pass2", 1_00_000);
 *
 *		https://gitlab.com/pravin/greatlearning.git
 */
	private static final Scanner sc = new Scanner(System.in);
	private static final BankingService service = new BankingService();

	public static void main(String[] args) {
		System.out.print("Enter account Number :");
		String acno = sc.nextLine();
		System.out.print("Enter Password :");
		String passwd = sc.nextLine();
		if (!service.authenticate(acno, passwd)) {
			System.out.println("invalid account number or password");
			return;
		}
		System.out.println("successful login");
		int choice = 0;
		do {
			choice = showMenuAndGetChoice();
			switch (choice) {
			case 1:
				depositOption(acno);
				break;
			case 2:
				withdrawOption(acno);
				break;
			case 3:
				transferOption(acno);
				break;
			case 0:
				System.out.println("Logging out");
				break;
			default:
				System.out.println("invalid option");
			}
		} while (choice != 0);
	}
	private static int showMenuAndGetChoice() {
		System.out.println("-----Menu-------");
		System.out.println("1. Deposit");
		System.out.println("2. Withdraw");
		System.out.println("3. Transfer");
		System.out.println("0. Logout");
		System.out.println("----------------");
//		String s = System.console().readLine("Enter your choice");
		return sc.nextInt();
	}
	private static void depositOption(String acno) {
		System.out.print("Enter amount to be deposited :");
		double amount = sc.nextDouble();
		service.deposit(acno, amount);
		System.out.println("Successfully deposited " + amount + "new balance "+ service.getBalance(acno));
	}
	private static void withdrawOption(String acno) {
		System.out.print("Enter amount to be Withdrawn :");
		double amount = sc.nextDouble();
		boolean success = service.withdraw(acno, amount);
		if (!success) {
			System.out.println("insufficient funds");
			return;
		}
		System.out.println("Successfully withdrawn " + amount + "new balance "+ service.getBalance(acno));
	}
	private static void transferOption(String acno) {
		String otp = OtpGenerator.generateOtp();
		System.out.println("Generated OTP:"+otp);
		System.out.print("Enter Otp Received :");
		String enteredOtp = sc.next();
		if (!otp.equals(enteredOtp)) {
			System.out.println("Incorrect OTP");
			return;
		}
		System.out.print("Enter amount to be transferred :");
		double amount = sc.nextDouble();
		System.out.print("Enter destination account number :");
		String destAcno = sc.next();

		boolean success = service.transfer(acno, destAcno, amount);
		if (!success) {
			System.out.println("insufficient funds");
			return;
		}
		System.out.println("Successfully withdrawn " + amount + " from "+ acno+" new balance "+ service.getBalance(acno));
		System.out.println("Successfully deposited" + amount + " to "+ destAcno+" new balance "+ service.getBalance(destAcno));
	}
}
